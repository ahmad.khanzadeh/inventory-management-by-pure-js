// template data stored 
const products=[
    {
        id:1,
        title:'milk',
        category:'Diary',
        updated:'2022-4-31t15:02:00.411z'
    },
    {
        id:2,
        title:'carrot',
        category:'vagtables',
        updated:'2022-4-31t15:02:00.411z'
    },
    {
        id:3,
        title:'lotus',
        category:'vagtables',
        updated:'2022-4-31t15:02:00.411z'
    },
];
const categories=[
    {
        id:1,
        name:'Diary',
        description:'including all milk products',
        createdAt:'2020-11-31t15:02:00.411z',
    },
    {
        id:2,
        name:'vagtables',
        description:'including all plants product',
        createdAt:'2021-10-31t15:02:00.411z',
    }
];
export default class Storage{
    // property
    // methods => save, update, delete , ..... ( CRUD Operation )
    // what are in this moduls
        // add new category
        // save category 
        // get all categories to user 

    // to prevent refrenced call for a sub function, we will make the get all categories method with static
    static getAllCategories(){
        // since it was a demedasti method , we make it by static( no new needed to call this function)
        // saved data  == > products, categories ( user local storage)
        // all data in local storage are a kind of string- to turn it into json, we need to use JSON.parse.if empty return a empty array by pipe
        const saveCategories=JSON.parse(localStorage.getItem('category')) || [];
       const sortedCategories= saveCategories.sort((a,b)=>{
            // return a > b ? -1 : 1
            return new Date(a.createdAt) > new Date(b.createdAt) ? -1 : 1;
        })
        // sort data  ===>desending
        return sortedCategories;
    }

    static saveCategory(categoryToSave){
        // name and description will be recived from user
        // code will make an id and a createdAt for the entered data
        const savedCategories = Storage.getAllCategories();
        // edit ==> ....save    ||   new ===> ...save
        const existedItem=savedCategories.find((c)=>c.id===categoryToSave.id);
           if (existedItem){
            //  edit
            existedItem.name=categoryToSave.name;
            existedItem.description=categoryToSave.description;
           }else{
            // add new item to the list 
            categoryToSave.id=new Date().getTime();
            categoryToSave.createdAt= new Date().toISOString();
            // push data to savedCategories
            categories.push(categoryToSave);
           }

        //    save this data into localstorage. Achtung! Wir muessen das existedItem nac string Uberweisen.
        localStorage.setItem('category',JSON.stringify(savedCategories));
    }
}